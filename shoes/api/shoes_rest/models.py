from enum import unique
from pyexpat import model
from django.db import models
from django.urls import reverse


class BinVO(models.Model):
  import_href = models.CharField(max_length=255,unique=True)
  closet_name = models.CharField(max_length=255)
  bin_number = models.PositiveSmallIntegerField()
  bin_size = models.PositiveSmallIntegerField()


class Shoe(models.Model):
  manufacturer = models.CharField(max_length=255)
  model_name = models.CharField(max_length=255)
  color = models.CharField(max_length=255)
  url = models.URLField()
  bin = models.ForeignKey(
    BinVO,
    related_name="bin",
    on_delete=models.CASCADE
  )

  def __str__(self):
    return self.model_name

  def get_api_url(self):
    return reverse("api_show_shoe", kwargs={"pk": self.pk})
